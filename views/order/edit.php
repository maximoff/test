<?php use yii\helpers\Url; ?>
<form action="<?= Url::to('/orders/save'); ?>" method="POST" id="ordersForm">
    <div style="float: left">
        <label for="order_id">Order Id</label>
        <input type="number"
               name="order_id"
               id="order_id"
               required
               value="<?= $orders['order_id']; ?>">
    </div>
    <div style="float: right">
        <button type="button" id="addRow">(+) Add row</button>&nbsp;
        <button id="submit">Submit</button>
    </div>
    <hr style="clear: both;">
    <div id="rows">
        <?php foreach ($orders['Order'] as $idx => $order): ?>
            <div class="row">
                <button type="button" class="deleteRow">(-) Delete row</button>
                <br><br>

                <input type="hidden"
                       data-name="id"
                       name="Order[<?= $idx; ?>][id]"
                       value="<?= $order['id']; ?>">

                <label>Price</label>
                <input type="number"
                       name="Order[<?= $idx; ?>][price]"
                       data-name="price"
                       required
                       value="<?= $order['price']; ?>">
                <br><br>

                <label>Description</label>
                <input type="text"
                       name="Order[<?= $idx; ?>][description]"
                       data-name="description"
                       required
                       value="<?= $order['description']; ?>">
                <br><br>

                <label>Available</label>
                <?php $checked = $order['available'] == 1 ? 'checked="checked"' : ''; ?>
                <input type="checkbox"
                       name="Order[<?= $idx; ?>][available]"
                       value="1"
                       data-name="available"
                       <?= $checked; ?>>
                <hr>
            </div>
        <?php endforeach; ?>
    </div>
</form>