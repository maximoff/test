<?php
use yii\helpers\Url;
?>
<a href="<?= Url::to('/orders');?>">Back to list of Order Id's</a>
<hr>
<table id="orderList">
    <thead>
    <tr>
        <th>Id</th>
        <th>Order Id</th>
        <th>Price</th>
        <th>Description</th>
        <th>Available</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($orders as $order): ?>
        <tr>
            <td><?= $order['id'] ?></td>
            <td><?= $order['order_id'] ?></td>
            <td><?= $order['price'] ?></td>
            <td><?= $order['description'] ?></td>
            <td><?= $order['available'] == 1 ? 'Yes' : 'No' ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

