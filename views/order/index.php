<?php use yii\helpers\Url; ?>
<a href="<?= Url::to('orders/add');?>">Add</a>
<hr>
<table id="orderIdList">
    <thead>
        <tr>
            <th>Order Id</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($orders as $order): ?>
        <tr>
            <td><?= $order['order_id'] ?></td>
            <td>
                <a href="<?= Url::to('orders/view/' . $order['order_id']);?>">View</a>&nbsp;
                <a href="<?= Url::to('orders/edit/' . $order['order_id']);?>">Edit</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

