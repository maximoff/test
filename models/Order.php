<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Order extends ActiveRecord
{
    public static function tableName()
    {
        return 'orders';
    }

    public function rules()
    {
        return [
            [['price', 'description'], 'required'],
            [['order_id', 'price', 'description', 'available'], 'safe'],
        ];
    }

}
