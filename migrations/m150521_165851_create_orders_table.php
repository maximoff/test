<?php

use yii\db\Schema;
use yii\db\Migration;

class m150521_165851_create_orders_table extends Migration
{
    public function up()
    {

        $this->createTable('orders', [
            'id' => Schema::TYPE_PK,
            'order_id' => Schema::TYPE_INTEGER,
            'price' => Schema::TYPE_DECIMAL . '(10,2) NOT NULL',
            'description' => Schema::TYPE_STRING . ' NOT NULL',
            'available' => 'tinyint(1)',
        ]);
    }

    public function down()
    {
        $this->dropTable('orders');
    }
}
