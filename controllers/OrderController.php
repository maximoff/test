<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Order;

class OrderController extends Controller
{
    public function actionIndex()
    {
        $orders = Order::find()->groupBy('order_id')->asArray()->all();;
        echo $this->render('index', array('orders' => $orders));
    }

    public function actionEdit($id = null)
    {
        if (is_null($id)) {
            $data = [
                'order_id' => null,
                'Order' => [[
                    'id' => null,
                    'order_id' => null,
                    'price' => null,
                    'description' => null,
                    'available' => null
                ]]
            ];
        } else {
            $data = (array)Order::findAll(['order_id' => $id]);
            $data = [
                'order_id' => $data[0]['order_id'],
                'Order' => $data
            ];
        }

        echo $this->render('edit', array('orders' => $data));
    }

    public function actionView($id = null)
    {
        $data = (is_null($id)) ? [] : (array)Order::findAll(['order_id' => $id]);
        echo $this->render('view', array('orders' => $data));
    }

    public function actionSave()
    {
        $request = Yii::$app->request->post();
        Order::deleteAll(['order_id' => $request['order_id']]);
        foreach ($request['Order'] as $idx => $order) {
            $model = new Order;
            $order['available'] = isset($order['available']) ? 1 : 0;
            $order['order_id'] = $request['order_id'];
            $model->setAttributes($order);
            $model->save();
        }

        $this->actionEdit($request['order_id']);
    }

}
