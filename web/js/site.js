$(document).ready(function () {

    $('#addRow').on('click', function () {
        var row = $('#rows .row:nth-child(1)').clone(true, true);
        $(row).find('input').val('').prop('checked', false);
        $('#rows').append(row);
        $('.row').each(function (idx) {
            $(this).find('input').each(function () {
                $(this).attr('name', 'Order[' + idx + '][' + $(this).data('name') + ']');
            });
        });
    });

    $('.row').on('click', '.deleteRow', function () {
        $(this).parent('.row').remove();
    });

    $('#submit').on('click', function () {
        $('#ordersForm').trigger('submit');
    });

    $('input[type="checkbox"]').on('change', function () {
        this.value = (Number(this.checked));
    });
});